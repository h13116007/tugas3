package tugas3;
import java.util.*;
@SuppressWarnings({ "unused", "resource" })	
public class OOPGeometri {

	public static void main(String[]arrrrgs){
		Scanner input=new Scanner(System.in);
		
		MainMenu mainMenu=new MainMenu();
		
		try{
			mainMenu.display();
		}catch(NoSuchElementException e){
			System.out.println();
			System.out.println("You pressed Ctrl + Z");
			System.out.println("Program Terminated");
		}
	}	

}

class MainMenu{	
	static Scanner input=new Scanner(System.in);	
	void display(){

		threeDimensional threeDimension=new threeDimensional();
		twoDimensional twoDimension=new twoDimensional();
		
		do{
			String choice="a";
	    	int select=-1;
	    System.out.println("\n");
		System.out.println("        2 and 3 Dimensional Geometry");
		System.out.println("========================================"); 		
		System.out.println("1. 2 Dimensional Geometry");
		System.out.println("2. 3 Dimensional Geometry");
		System.out.println("0. Exit");
		System.out.println("========================================");
			System.out.print("Please select a number : ");
			try{
				select=input.nextInt();	    			
			}catch(InputMismatchException e){
				System.out.println("That is not an integer");
				System.out.print("And ");
			}	    		

		 input.nextLine();			
		    	
		switch(select){
		case 0:
			break;
		case 1:
			twoDimension.display();
			break;
		case 2:   			
	        threeDimension.display();
	        break;
		default:
			System.out.println("That is not a correct choice");
			do{ 
			    System.out.print("Try again ? (y/n)");
			    System.out.println();			   
			    choice=input.nextLine();
			    System.out.println("----------------------------------------");
			    
				    if (choice.contentEquals("y")){
				    	break;
				    }
				    else if(choice.contentEquals("n")){
				    	break;
				    }
				    else {
				    	System.out.println("Please input only the letter 'y' or 'n'\n" );
				    }
				    
	    	}while( (choice.contentEquals("y") == false ) || ( choice.contentEquals("n") == false) );
			break;
	   }
				if (select == 0)break;
			    if (choice.contentEquals("n"))break;
			    if (choice.contentEquals("y"))continue;
		
		}while(true);
	}
}
class twoDimensional extends MainMenu{	 
	 static Scanner input=new Scanner(System.in);	 
	 void display(){
		  
		  rectangle rectangle = new rectangle();
		  isoTriangle isoTriangle = new isoTriangle();
		  equiTriangle equiTriangle = new equiTriangle();
		  square square = new square();
		  parallelogram parallelogram = new parallelogram();
		  trapezoid trapezoid = new trapezoid();
		  circle circle = new circle();
		  ellipse ellipse = new ellipse();
		  polygon polygon = new polygon();
 
			do{
				String choice="a";
		    	int select=-1;
		    	System.out.println("\n");
				System.out.println("        Two Dimensional Geometry");
				System.out.println("========================================"); 		
				System.out.println("1. Rectangle (Persegi Panjang)");
				System.out.println("2. Isosceles Triangle (Segitiga Sama Kaki)");
				System.out.println("3. Equilateral Triangle (Segitiga Sama Sisi)");
				System.out.println("4. Square (Persegi)");
				System.out.println("5. Parallelogram (Jajar Genjang)");
				System.out.println("6. Trapezoid (Trapesium)");
				System.out.println("7. Circle (Lingkaran)");
				System.out.println("8. Ellipse (Elips)");
				System.out.println("9. Regular Polygon (Poligon)");
				System.out.println("0. Back to Main Menu");
				System.out.println("========================================");
					System.out.print("Please select a number : ");
		    		try{
		    			select=input.nextInt();
		    			System.out.println("----------------------------------------");
		    		}catch(InputMismatchException e){
		    			System.out.println("That is not an integer");
		    			System.out.print("And ");
		    		}	    		
			
				 input.nextLine();			
				    	
				switch(select){
				case 0:
					break;
				case 1:
					rectangle.calculate();
					break;
				case 2:   			
		            isoTriangle.calculate();
		            break;
				case 3:
					equiTriangle.calculate();
					break;
				case 4:
					square.calculate();
					break;
				case 5:
					parallelogram.calculate();
					break;
				case 6:
					trapezoid.calculate();
					break;
				case 7:
					circle.calculate();
					break;
				case 8:
					ellipse.calculate();
					break;
				case 9:
					polygon.calculate();
					break;
				default:
					System.out.println("That is not a correct choice");
					do{ 
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
					    
						    if (choice.contentEquals("y")){
						    	break;
						    }
						    else if(choice.contentEquals("n")){
						    	break;
						    }
						    else {
						    	System.out.println("Please input only the letter 'y' or 'n'\n" );
						    }
						    
			    	}while( (choice.contentEquals("y") == false ) || ( choice.contentEquals("n") == false) );
					break;
			   }
				
						if (select == 0)break;
					    if (choice.contentEquals("n"))break;
					    if (choice.contentEquals("y"))continue;
				
			}while(true);
		}
}
class threeDimensional extends MainMenu{	 
	 static Scanner input=new Scanner(System.in);
	 void display(){
		  
	  	 cube cube = new cube();
	   	 rectSolid rectSolid = new rectSolid();
		 sphere sphere = new sphere();
		 cylinder cylinder = new cylinder();
		 torus torus = new torus();
		 circCone circCone = new circCone();
		 frustum frustum = new frustum();
		 squarePyramid squarePyramid = new squarePyramid();
		 tetrahedron tetrahedron = new tetrahedron();
		 
			do{
				
	 		String choice="a";
	     	int select=-1;
	     	System.out.println("\n");
	 		System.out.println("        Three Dimensional Geometry");
	 		System.out.println("========================================"); 		
	 		System.out.println("1. Cube (Kubus)");
	 		System.out.println("2. Rectangular Solid (Balok)");
	 		System.out.println("3. Sphere (Bola)");
	 		System.out.println("4. Right Circular Cylinder (Tabung)");
	 		System.out.println("5. Torus (Ban)");
	 		System.out.println("6. Right Circular Cone (Kerucut)");
	 		System.out.println("7. Frustum of a Cone (Potongan Kerucut)");
	 		System.out.println("8. Square Pyramid (Limas Segiempat)");
	 		System.out.println("9. Regular Tetrahedron(Limas Segitiga)");
	 		System.out.println("0. Back to Main Menu");
	 		System.out.println("========================================");
	 			System.out.print("Please select a number : ");
		    		try{
		    			select=input.nextInt();
		    			System.out.println("----------------------------------------");
		    		}catch(InputMismatchException e){
		    			System.out.println("That is not an integer");
		    			System.out.print("And ");
		    		}	    		
	 	
	 		 input.nextLine();			
	 		
	 		switch(select){
	 		case 0:
	 			break;
	 		case 1:
	 			cube.calculate();
	 			break;
	 		case 2:   			
	             rectSolid.calculate();
	             break;
	 		case 3:
	 			sphere.calculate();
	 			break;
	 		case 4:
	 			cylinder.calculate();
	 			break;
	 		case 5:
	 			torus.calculate();
	 			break;
	 		case 6:
	 			circCone.calculate();
	 			break;
	 		case 7:
	 			frustum.calculate();
	 			break;
	 		case 8:
	 			squarePyramid.calculate();
	 			break;
	 		case 9:
	 			tetrahedron.calculate();
	 			break;
	 			
	 		default:
	 			System.out.println("That is not a correct choice");
	 			do{ 
	 			    System.out.print("Try again ? (y/n)");
	 			    System.out.println();			   
	 			    choice=input.nextLine();
	 			    System.out.println("----------------------------------------");
	 			    
	 				    if (choice.contentEquals("y")){
	 				    	break;
	 				    }
	 				    else if(choice.contentEquals("n")){
	 				    	break;
	 				    }
	 				    else {
	 				    	System.out.println("Please input only the letter 'y' or 'n'\n" );
	 				    }
	 				    
	 	    	}while( (choice.contentEquals("y") == false ) || ( choice.contentEquals("n") == false) );
	 			break;
	 			
	 	   }
	 	   
	 				if (select == 0)break;
	 			    if (choice.contentEquals("n"))break;
	 			    if (choice.contentEquals("y"))continue;
	 		
	 		
	 			
	 	}while(true); 
	 }
}
 

class cubeFormula{
		double side;
		double volume(){
			return side*side*side;
		}
		double surfArea(){
			return side*side*6.0;
		}
}
		class cube extends cubeFormula{
				static Scanner input=new Scanner(System.in);
				 void calculate(){
				double side;
			    String choice;
			    do{
				    try{
				    	System.out.println(" Cube ");
				    	System.out.println("Please input the length of the side");
				    	System.out.print(">> ");
				    	side=input.nextDouble();
				    	this.side=side;
				    	if (this.side <= 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The volume of the cube is %.1f\n",volume());
				    	
				    	System.out.printf("The surface area of the cube is %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("It isn't a number");
				    }
				   
				    catch(IllegalArgumentException e){
				    	if(this.side<0)System.out.println("Numbers cannot be negative");
				    	else if(this.side==0)System.out.println("Numbers cannot be zero");
				    	
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if ( choice.contentEquals("y") )break;
						    else if( choice.contentEquals("n") )break;
						    else System.out.println("Please input only the letter 'y' or 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || ( choice.contentEquals("n") ) == false);
				    
				    if ( choice.contentEquals("y") )continue;
				    else if( choice.contentEquals("n") )break;
					    
				    
			    }while(true);
				}
		}
	
class rectSolidFormula{
	 double length,width,height;
	 double volume(){
		 return length*width*height;
	 }
	 double surfArea(){
		 return (2 * length * width) + (2 * length * height) + (2 * width * height);
	 }
}	
		class rectSolid extends rectSolidFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
				double length,width,height;
		        String choice;
		        do{
				    try{
				    	System.out.println("Rectangular Solid");
				    	System.out.println("Please input the length of the Rectangular Solid");
				    	System.out.print(">> ");
				    	length=input.nextDouble();  	
				    	this.length=length;
				    	if (length < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (length == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Please input the width of the Rectangular Solid");
				    	System.out.print(">> ");
				    	width=input.nextDouble();  
				    	this.width=width;
				    	if (width < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (width == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Please input the height of the Rectangular Solid");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
				    	if (height < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (height == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The volume of the Rectangular Solid is %.1f\n",volume());
			
				    	System.out.printf("The surface area of the Rectangular Solid is %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("It isn't a number");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Cannot divide by zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Numbers cannot be negative");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Please input only the letter 'y' or 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
		        }while(true);
			}
		}

class sphereFormula{
	double pi=(double)22/7,radius;
	double volume(){
		return ( (double) 4/3 ) * pi * radius * radius * radius;
	}
	double surfArea(){
		return 4.0 * pi * radius * radius;
	}
}
		class sphere extends sphereFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
		
				double radius;
			    String choice;
			    do{
				    try{
				    	System.out.println("                 Sphere");
				    	System.out.println("Please input the radius of the Sphere");
				    	System.out.print(">> ");
				    	radius=input.nextDouble();
				    	this.radius=radius;
				    	if (radius < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (radius == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    		
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The volume of the Sphere is %.2f\n",volume());
				    	
				    	System.out.printf("The surface area of the Sphere is %.2f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("It isn't a number");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Cannot divide by zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Numbers cannot be negative");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Please input only the letter 'y' or 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}

class cylinderFormula{
	double pi=(double)22/7,radius,height;
	double volume(){
		return pi * radius * radius * height;
	}
	double surfArea(){
		return (2.0 * pi * radius * height) + (2.0 * pi * radius * radius);
	}
}
		class cylinder extends cylinderFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
				double radius,height;
			    String choice;
			    do{
				    try{
				    	System.out.println("Right Circular Cylinder");
				    	System.out.println("Please input the radius of the base of the Right Circular Cylinder");
				    	System.out.print(">> ");
				    	radius=input.nextDouble();
				    	this.radius=radius;
				    	if (radius < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (radius == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Please input the height of the Right Circular Cylinder");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
				    	if (height < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (height == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The volume of the Right Circular Cylinder is %.1f\n",volume());
				    	
				    	
				    	System.out.printf("The surface area of the Right Circular Cylinder is %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("It isn't a number");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Cannot divide by zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Numbers cannot be negative");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Please input only the letter 'y' or 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}



class torusFormula{
	double pi=(double)22/7,tubeRadius,torusRadius;
	double volume(){
		return (double)2.0 * pi * pi * tubeRadius * tubeRadius * torusRadius;
	}
	double surfArea(){
		return (double)4 * pi * pi * tubeRadius * torusRadius;
	}
}
		class torus extends torusFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
				double tubeRadius,torusRadius;
			    String choice;
			    do{
				    try{
				    	System.out.println("Torus");
				    	System.out.println("Please input the tube radius of the Torus");
				    	System.out.print(">> ");
				    	tubeRadius=input.nextDouble();
				    	this.tubeRadius=tubeRadius;
				    	if (tubeRadius < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (tubeRadius == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Please input the radius of the Torus");
				    	System.out.print(">> ");
				    	torusRadius=input.nextDouble();
				    	this.torusRadius=torusRadius;
				    	if (torusRadius < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (torusRadius == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The volume of the Torus is %.1f\n",volume());
				    	
				    	
				    	System.out.printf("The surface area of the Torus is %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("It isn't a number");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Cannot divide by zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Numbers cannot be negative");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Please input only the letter 'y' or 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}



class circConeFormula{
	double pi = (double)22/7,radius,height;
	double volume(){
		return ((double)1/3) * pi * radius * radius * height;
	}
	double surfArea(){
		return pi * radius * (Math.sqrt((radius * radius) + (height * height))) + (pi * radius * radius);
	}
	
}
		class circCone extends circConeFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
				double radius,height;
			    String choice;
			    do{
				    try{
				    	System.out.println(" Circular Cone");
				    	System.out.println("Please input the radius of the Cone");
				    	System.out.print(">> ");
				    	radius=input.nextDouble();  	
				    	this.radius=radius;
				    	if (radius < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (radius == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Please input the height of the Cone");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
				    	if (height < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (height == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The volume of the Cone is %.1f\n",volume());
				    	
				    	System.out.printf("The surface area of the Cone is %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("It isn't a number");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Cannot divide by zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Numbers cannot be negative");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Please input only the letter 'y' or 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}



class frustumFormula{
	double pi = (double)22/7,topRadius,slantHeight,height,baseRadius;
	double volume(){
		return ((double)pi/3) * ( (topRadius * topRadius) + (topRadius * baseRadius) + (baseRadius * baseRadius)) * height;
	}
	double surfArea(){
		return (pi * slantHeight * (topRadius + baseRadius) ) + (pi * topRadius * topRadius) + (pi * baseRadius * baseRadius);
	}
}
		class frustum extends frustumFormula{
			static Scanner input=new Scanner(System.in);
			void calculate(){
		
				double baseRadius,height,topRadius,slantHeight;
			    String choice;
			    do{
				    try{
				    	System.out.println(" Frustum of a Cone");
				    	System.out.println("Please input the base radius of the Frustum of the Cone");
				    	System.out.print(">> ");
				    	baseRadius=input.nextDouble(); 
				    	this.baseRadius=baseRadius;
				    	if (baseRadius < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (baseRadius == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Please input the top radius of the Frustum of the Cone");
				    	System.out.print(">> ");
				    	topRadius=input.nextDouble();
				    	this.topRadius=topRadius;
				    	if (topRadius < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (topRadius == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	System.out.println("Please input the height of the Frustum of the Cone");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
				    	if (height < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (height == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	System.out.println("Please input the Slant Height of the Frustum of the Cone");
				    	System.out.print(">> ");
				    	slantHeight=input.nextDouble();
				    	this.slantHeight=slantHeight;
				    	if (slantHeight < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (slantHeight == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The volume of the Frustum of the Cone is %.1f\n",volume());
				    	
				    	System.out.printf("The surface area of the Frustum of the Cone is %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("It isn't a number");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Cannot divide by zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Numbers cannot be negative");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Please input only the letter 'y' or 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}



class squarePyramidFormula{
	double side,height;
	double volume(){
		return (double)1/3 * side * side * height;
	}
	double surfArea(){
		return side * (side + Math.sqrt( (side * side) + (4 * height * height) ) );
	}
}
		class squarePyramid extends squarePyramidFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
		
				double side,height;
			    String choice;
			    do{
				    try{
				    	System.out.println("Square Pyramid");
				    	System.out.println("Please input the base side length of the Square Pyramid");
				    	System.out.print(">> ");
				    	side=input.nextDouble();
				    	this.side=side;
				    	if (side < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (side == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	
				    	System.out.println("Please input the height of the Square Pyramid");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
				    	if (height < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (height == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The volume of the Square Pyramid is %.1f\n",volume());
				    	
				    	System.out.printf("The surface area of the Square Pyramid is %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("It isn't a number");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Cannot divide by zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Numbers cannot be negative");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Please input only the letter 'y' or 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}



class tetrahedronFormula{
	double side;
	double volume(){
		return ((double)1/12) * Math.sqrt(2 * side * side * side);
	}
	double surfArea(){
		return Math.sqrt(3.0 * side * side);
	}
}
		class tetrahedron extends tetrahedronFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
		
				double side;
			    String choice;
			    do{
				    try{
				    	System.out.println("Regular Tetrahedron");
				    	System.out.println("Please input the number of side length of the Tetrahedron");
				    	System.out.print(">> ");
				    	side=input.nextDouble();  	
				    	if (side < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (side == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The volume of the Tetrahedron is %.1f\n",volume());
				    	
				    	System.out.printf("The surface area of the Tetrahedron is %.1f\n",surfArea());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("It isn't a number");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Cannot divide by zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Numbers cannot be negative");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Please input only the letter 'y' or 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}



class rectangleFormula{
	double width,length;
	double area(){
		return width*length;
	}
	double perimeter(){
		return (2.0 * width) + (2.0 * length);
	}

}
		class rectangle extends rectangleFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
			    double width,length;
			    String choice;
			    do{
				    try{
				    	System.out.println("Rectangle");
				    	System.out.println("Please input the size of the width");
				    	System.out.print(">> ");
				    	width=input.nextDouble();
				    	this.width=width;
				    	if (width < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (width == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Please input the size of the length");
				    	System.out.print(">> ");
				    	length=input.nextDouble();
				    	this.length=length;
				    	;
				    	if (length < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (length == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The area of the rectangle is %.1f\n",area());
				    	
				    	System.out.printf("The perimeter of the rectangle is %.1f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("It isn't a number");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Cannot divide by zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Numbers cannot be negative");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Please input only the letter 'y' or 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
					    
				    
			    }while(true);
			}
		}



class isoTriangleFormula{
	double base,height,anotherside;
	double area(){
		return 0.5 * base * height;
	}
	double perimeter(){
		return base + (2.0 * anotherside);
	}
}
		class isoTriangle extends isoTriangleFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
			    double base,height,anotherside;
			    String choice;
			    do{
				    try{
				    	System.out.println("Isosceles Triangle");
				    	System.out.println("Please input the size of the base");
				    	System.out.print(">> ");
				    	base=input.nextDouble();
				    	this.base=base;
				    	if (base < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (base == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Please input the size of the height");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
				    	if (height < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (height == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Please input the size of side B (B = C)");
				    	System.out.print(">> ");
				    	anotherside=input.nextDouble();
				    	
				    	if (anotherside < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (anotherside == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The area of the Isosceles Triangle is %.1f\n",area());
				    	
				    	System.out.printf("The perimeter of the Isosceles Triangle is %.1f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("It isn't a number");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Numbers cannot be zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Numbers cannot be negative");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Please input only the letter 'y' or 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			}
		}



class equiTriangleFormula{
	double side,height;
	double area(){
		return 0.5 * side * height;
	}
	double perimeter(){
		return side + side + side;
	}
}
		class equiTriangle extends equiTriangleFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
			    double side,height;
			    String choice;
			    do{
				    try{
				    	System.out.println("Equilateral Triangle");
				    	System.out.println("Please input the size of the side");
				    	System.out.print(">> ");
				    	side=input.nextDouble();
				    	this.side=side;
				    	if (side < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	if (side == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	System.out.println("Please input the size of the height");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
				    	
				    	if (height < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	
				    	if (height == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The area of the Equilateral Triangle is %.1f\n",area());
				    	
				    	System.out.printf("The perimeter of the Equilateral Triangle is %.1f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("It isn't a number");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Numbers cannot be zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Numbers cannot be negative");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Please input only the letter 'y' or 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			}
		}



class squareFormula{
	double side;
	double area(){
		return side * side ;
	}
	double perimeter(){
		return side + side + side + side;
	}
}
		class square extends squareFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
		
			    double side;
			    String choice;
			    do{
				    try{
				    	System.out.println("Square");
				    	System.out.println("Please input the size of the side");
				    	System.out.print(">> ");
				    	side=input.nextDouble();
				    	this.side=side;
				    	
				    	
				    	if (side < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	
				    	if (side == 0) {
				    	    throw new ArithmeticException();
				    	}	
				    	
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The area of the Square is %.1f\n",area());
				    	
				    	System.out.printf("The perimeter of the square is %.1f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("It isn't a number");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Numbers cannot be zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Numbers cannot be negative");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Please input only the letter 'y' or 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			}
		}



class parallelogramFormula{
	double base,height,anotherside;
	double area(){
		return base * height;
	}
	double perimeter(){
		return (2.0 * base ) + (2.0 * anotherside);
	}
}
		class parallelogram extends parallelogramFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				double base,height,anotherside;
			    String choice;
			    do{
				    try{
				    	System.out.println("Parallelogram");
				    	System.out.println("Please input the size of the base");
				    	System.out.print(">> ");
				    	base=input.nextDouble();
				    	this.base=base;
					    	if (base < 0) {
					    	    throw new IllegalArgumentException();
					    	}
					    	if (base == 0) {
					    	    throw new ArithmeticException();
					    	}	
				    	
				    	System.out.println("Please input the size of the height");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
					    	if (height < 0) {
					    	    throw new IllegalArgumentException();
					    	}
					    	if (height == 0) {
					    	    throw new ArithmeticException();
					    	}	
				    	
				    	System.out.println("Please input the size of side B");
				    	System.out.print(">> ");
				    	anotherside=input.nextDouble();
				    	this.anotherside=anotherside;
					    	if (anotherside < 0) {
					    	    throw new IllegalArgumentException();
					    	}	    	
					    	if (anotherside == 0) {
					    	    throw new ArithmeticException();
					    	}
					    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The area of the Parallelogram is %.1f\n",area());
				    	
				    	System.out.printf("The perimeter of the Parallelogram is %.1f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("It isn't a number");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Numbers cannot be zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Numbers cannot be negative");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Please input only the letter 'y' or 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			}
		}



class trapezoidFormula{
	double base1,base2,height,anotherside;
	double area(){
		return 0.5 * (base1 + base2) * height;
	}
	double perimeter(){
		return base1 + base2 + (2.0 * anotherside);
	}
}
		class trapezoid extends trapezoidFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
		
			    double base1,base2,height,anotherside;
			    String choice;
			    do{
				    try{
				    	System.out.println("Trapezoid");
				    	System.out.println("Please input the size of the base (bottom)");
				    	System.out.print(">> ");
				    	base1=input.nextDouble();
				    	this.base1=base1;
					    	if (base1 < 0) {
					    	    throw new IllegalArgumentException();
					    	}if (base1 == 0) {
					    	    throw new ArithmeticException();
					    	}
				    	
				    	System.out.println("Please input the size of the base (top)");
				    	System.out.print(">> ");
				    	base2=input.nextDouble();
				    	this.base2=base2;
					    	if (base2 < 0) {
					    	    throw new IllegalArgumentException();
					    	}if (base2 == 0) {
					    	    throw new ArithmeticException();
					    	}
					    	
				    	System.out.println("Please input the size of the height");
				    	System.out.print(">> ");
				    	height=input.nextDouble();
				    	this.height=height;
					    	if (height < 0) {
					    	    throw new IllegalArgumentException();
					    	}if (height == 0) {
					    	    throw new ArithmeticException();
					    	}	
				    	
				    	System.out.println("Please input the size of side C (C = D)");
				    	System.out.print(">> ");
				    	anotherside=input.nextDouble();
				    	this.anotherside=anotherside;
					    	if (anotherside < 0) {
					    	    throw new IllegalArgumentException();
					    	}		    	
					    	if (anotherside == 0) {
					    	    throw new ArithmeticException();
					    	}
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The area of the Trapezoid is %.1f\n",area());
				    	
				    	System.out.printf("The perimeter of the Trapezoid is %.1f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("It isn't a number");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Numbers cannot be zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Numbers cannot be negative");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Please input only the letter 'y' or 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			    
			}
		}



class circleFormula{
	double radius,pi=(double)22/7;
	double area(){
		return pi * radius * radius;
	}
	double perimeter(){
		return 2.0 * pi * radius;
	}
}
		class circle extends circleFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
		
			    double radius;
			    String choice;
			    do{
				    try{
				    	System.out.println("Circle");
				    	System.out.println("Please input the size of the radius");
				    	System.out.print(">> ");
				    	radius=input.nextDouble();
		
				    	if (radius < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (radius == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	
				    	System.out.println();
				    	
				    
				    	System.out.printf("The area of the Circle is %.3f\n",area());
				    	
				    	System.out.printf("The circumference of the Circle is %.3f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("It isn't a number");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Numbers cannot be zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Numbers cannot be negative");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Please input only the letter 'y' or 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			}
		}



class ellipseFormula{
	double semiMajor,semiMinor,pi=(double)22/7;
	double area(){
		return pi * semiMajor *  semiMinor ;
	}
	double circumference(){
		return pi * ((3.0 * (semiMajor + semiMinor)) - Math.sqrt(((semiMajor + (3.0 * semiMinor)) * ((3 * semiMajor) + semiMinor))));
	}
}
		class ellipse extends ellipseFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
				
				double semiMajor,semiMinor;
			    String choice;
			    do{
				    try{
				    	System.out.println("Ellipse");
				    	System.out.println("Please input the size of the Semimajor Axis");
				    	System.out.print(">> ");
				    	semiMajor=input.nextDouble();
				    	this.semiMajor = semiMajor;
				    	if (semiMajor < 0) {
				    	    throw new IllegalArgumentException();
				    	}if (semiMajor == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	
				    	System.out.println("Please input the size of the Semiminor Axis");
				    	System.out.print(">> ");
				    	semiMinor=input.nextDouble();
				    	this.semiMinor= semiMinor;
				    	if (semiMinor < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	
				    	if (semiMinor == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The area of the Ellipse is %.3f\n",area());
				    	
				    	System.out.printf("The circumference of the Ellipse is %.3f\n",circumference());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("It isn't a number");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Numbers cannot be zero");
				    }
				    catch(IllegalArgumentException e){
				    	System.out.println("Numbers cannot be negative");
				    }
				    input.nextLine();
				    do{ 
				    	System.out.println();
					    System.out.print("Try again ? (y/n)");
					    System.out.println();			   
					    choice=input.nextLine();
					    System.out.println("----------------------------------------");
						    if (choice.contentEquals("y"))break;
						    else if(choice.contentEquals("n"))break;
						    else System.out.println("Please input only the letter 'y' or 'n'\n" );
				    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
				    
				    if (choice.contentEquals("y"))continue;
				    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			}
		}



class polygonFormula{
	double nsides,sideLength,pi=(double)22/7;
	double area(){
		return 0.25 * nsides * sideLength * sideLength * ((double)1 / Math.tan(pi/nsides));
	}
	double perimeter(){
		return nsides * sideLength;
	}
}
		class polygon extends polygonFormula{
			static Scanner input = new Scanner(System.in);
			void calculate(){
		
				double nsides=-1,sideLength=-1;
			    String choice;
			    do{
				    try{
				    	System.out.println("Regular Polygon");
				    	System.out.println("Please input the number of sides of the polygon ( >4 )");
				    	System.out.print(">> ");
				    	nsides=input.nextInt();
				    	
				    	if (nsides < 5) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	System.out.println("Please input the length of each side");
				    	System.out.print(">> ");
				    	sideLength=input.nextDouble();	    	
				    	if (sideLength < 0) {
				    	    throw new IllegalArgumentException();
				    	}
				    	
				    	if (sideLength == 0) {
				    	    throw new ArithmeticException();
				    	}
				    	
				    	System.out.println();
				    	
				    	
				    	System.out.printf("The area of the Polygon is %.3f\n",area());
				    	
				    	System.out.printf("The perimeter of the Polygon is %.1f\n",perimeter());
				    }
				    catch(InputMismatchException e){
				    	System.out.println("Error: The number of sides must be an integer");
				    	System.out.println("Error: It isn't a number");
				    }
				    catch(ArithmeticException e){
				    	System.out.println("Error: Numbers cannot be zero");
				    }
				    catch(IllegalArgumentException e){
				    	if(nsides<5 && nsides>=0)System.out.println("Error: The number of sides is less than 5");
				    	if(nsides<0 || sideLength<0)System.out.println("Error: Numbers cannot be negative ");
				    	
				    }
				    
				    input.nextLine();
					    do{ 
					    	System.out.println();
						    System.out.print("Try again ? (y/n)");
						    System.out.println();			   
						    choice=input.nextLine();
						    System.out.println("----------------------------------------");
							    if (choice.contentEquals("y"))break;
							    else if(choice.contentEquals("n"))break;
							    else System.out.println("Please input only the letter 'y' or 'n'\n" );
					    }while( (choice.contentEquals("y") == false) || (choice.contentEquals("n") ) == false);
					    
					    if (choice.contentEquals("y"))continue;
					    else if(choice.contentEquals("n"))break;
				   
			    }while(true);
			}
		}




